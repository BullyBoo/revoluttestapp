package ru.bullyboo.currency.repositories

import ru.bullyboo.currency.models.Rates
import ru.bullyboo.currency.network.RateApi
import ru.bullyboo.currency.network.Request
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RateRepository @Inject constructor(private val api : RateApi) {

    private val request = Request<Rates>()

    fun subscribe() =
            request.getLiveData()

    fun sendRequest(base : String) =
            request.setApiCall{api.getRate(base)}
                    .process()
}