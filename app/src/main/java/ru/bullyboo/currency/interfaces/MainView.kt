package ru.bullyboo.currency.interfaces

import android.support.annotation.StringRes
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface MainView : MvpView {

    fun setRecyclerVisibility(visibility : Int)

    fun setDescriptionVisibility(visibility : Int)

    fun setProgressVisibility(visibility: Int)

    @StateStrategyType(SkipStrategy::class)
    fun showToast(@StringRes stringId : Int)
}