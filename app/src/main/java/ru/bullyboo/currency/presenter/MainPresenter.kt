package ru.bullyboo.currency.presenter

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.content.IntentFilter
import android.view.View
import com.arellomobile.mvp.InjectViewState
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import ru.bullyboo.currency.R
import ru.bullyboo.currency.dagger.annotations.ActivityScope
import ru.bullyboo.currency.enums.Currency
import ru.bullyboo.currency.interfaces.MainView
import ru.bullyboo.currency.models.Rate
import ru.bullyboo.currency.models.Rates
import ru.bullyboo.currency.network.Response
import ru.bullyboo.currency.receivers.ConnectionReceiver
import ru.bullyboo.currency.repositories.RateRepository
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@ActivityScope
@InjectViewState
class MainPresenter @Inject constructor(private val repository : RateRepository): BasePresenter<MainView>() {

    private var hasData = false

    private var previousStatus = ConnectionReceiver.Status.NO_INFO

    val receiver = ConnectionReceiver(object : ConnectionReceiver.OnStateChanged{
        override fun onChanged(status: ConnectionReceiver.Status) {
            if(previousStatus == status){
                return
            }

            if(status == ConnectionReceiver.Status.CONNECTED){
                startListen()

                if(hasData && hasView()){
                    viewState.showToast(R.string.connection_restore)
                    viewState.setDescriptionVisibility(View.GONE)
                    viewState.setRecyclerVisibility(View.VISIBLE)
                }
            } else {
                stopListen()

                if(hasView()){
                    if(hasData){
                        viewState.showToast(R.string.connection_lost)
                    } else {
                        viewState.setProgressVisibility(View.GONE)
                        viewState.setDescriptionVisibility(View.VISIBLE)
                        viewState.setRecyclerVisibility(View.GONE)
                    }

                }
            }

            previousStatus = status
        }
    })

    var intentFilter : IntentFilter = IntentFilter()

    init {
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED")
    }

    private var baseCurrency = Currency.EUR

    var count = MutableLiveData<String>().apply {
        value = "100"
    }

    var countDigit = Transformations.map(count){
        try {
            it.toDouble()
        } catch (e: Exception) {
            1.0
        }
    }!!

    /**
     * Method for counting rate with multiplier, and beautify the result
     */
    fun multiplyRate(value : Double) : String {
        val d = countDigit.value

        if(d != null){
            val result = d.times(value)

            if(result.rem(1) == 0.0){
                return result.toInt().toString()
            }

            return (Math.round(result.times(100.0)) / 100.0).toString()
        } else {
            return ""
        }
    }

    private var disposable: Disposable? = null

    /**
     * Change the base currency
     * It`s item click in recycler view
     */
    fun onBaseChanged(rate: Rate){
        baseCurrency = rate.currency

        stopListen()
        startListen()
    }

    /**
     * Convert the result to list with a base currency
     */
    fun subscribe() = Transformations.map(repository.subscribe()){
        if(it != null){
            var list = Collections.emptyList<Any>()
            if(it.status == Response.Status.SUCCESS){

                if(!hasData){
                    viewState.setProgressVisibility(View.GONE)
                    viewState.setDescriptionVisibility(View.GONE)
                    viewState.setRecyclerVisibility(View.VISIBLE)
                }

                hasData = true
                list = createList(it.model!!)

            } else if(it.status == Response.Status.ERROR) {
                list = createList(it.model!!)

                if(hasView() && !hasData){
                    viewState.setProgressVisibility(View.GONE)
                    viewState.setDescriptionVisibility(View.VISIBLE)
                    viewState.setRecyclerVisibility(View.GONE)
                }
            } else {
                if(hasView() && !hasData){
                    viewState.setProgressVisibility(View.VISIBLE)
                    viewState.setDescriptionVisibility(View.GONE)
                }
            }
            list
        } else {
            Collections.emptyList()
        }
    }!!

    private fun startListen(){
        if(disposable != null){
            disposable!!.dispose()
        }

        disposable = Observable.interval(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    repository.sendRequest(baseCurrency.shortName)
                }
    }

    private fun stopListen(){
        if(disposable != null) {
            disposable!!.dispose()
        }
    }

    private fun createList(rates : Rates) : List<Any?>{
        val list = ArrayList<Any>()

        list.add(0, baseCurrency)
        list.addAll(rates.ratesList)

        return list
    }
}