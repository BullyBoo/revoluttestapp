package ru.bullyboo.currency.presenter

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView

abstract class BasePresenter<V : MvpView> : MvpPresenter<V>() {

    fun hasView() = viewState != null
}