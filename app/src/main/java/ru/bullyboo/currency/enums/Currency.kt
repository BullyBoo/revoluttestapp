package ru.bullyboo.currency.enums

import android.support.annotation.DrawableRes
import ru.bullyboo.currency.R

enum class Currency constructor(val id : Long, val shortName : String, val currencyName : String,
                                @DrawableRes val drawableId : Int){

    GBP(0, "GBP", "British Pound", R.drawable.gb),
    EUR(1, "EUR", "Euro", R.drawable.eu),
    USD(2, "USD", "US Dollar", R.drawable.us),
    AUD(3, "AUD", "Australian Dollar", R.drawable.au),
    BRL(4, "BRL", "Brazilian Real", R.drawable.br),
    BGN(5, "BGN", "Bulgarian Lev", R.drawable.bg),
    CAD(6, "CAD", "Canadian Dollar", R.drawable.ca),
    CNY(7, "CNY", "Chinese Yuan Renminbi", R.drawable.cn),
    HRK(8, "HRK", "Croatian Kuna", R.drawable.hr),
    CZK(9, "CZK", "Czech Koruna", R.drawable.cz),
    DKK(11, "DKK", "Danish Krone", R.drawable.dk),
    HKD(12, "HKD", "Hong Kong Dollar", R.drawable.hk),
    HUF(13, "HUF", "Hungarian Forint", R.drawable.hu),
    ISK(14, "ISK", "Icelandic Krona", R.drawable.`is`),
    INR(15, "INR", "Indian Rupee", R.drawable.`in`),
    IDR(16, "IDR", "Indonesian Rupiah", R.drawable.id),
    ILS(17, "ILS", "Israeli New Sheqel", R.drawable.il),
    JPY(18, "JPY", "Japanese Yen", R.drawable.jp),
    MYR(19, "MYR", "Malaysian Ringgit", R.drawable.my),
    MXN(20, "MXN", "Mexican Peso", R.drawable.mx),
    NZD(21, "NZD", "New Zealand Dollar", R.drawable.nz),
    NOK(22, "NOK", "Norwegian Krona", R.drawable.no),
    PHP(23, "PHP", "Philippine Piso", R.drawable.ph),
    PLN(24, "PLN", "Polish Zloty", R.drawable.pl),
    RON(25, "RON", "Romanian Leu", R.drawable.ro),
    RUB(26, "RUB", "Russian Ruble", R.drawable.ru),
    SGD(27, "SGD", "Singapore Dollar", R.drawable.sg),
    ZAR(28, "ZAR", "South African Rand", R.drawable.za),
    KRW(29, "KRW", "South Korean Won", R.drawable.kr),
    SEK(30, "SEK", "Swedish Krona", R.drawable.se),
    CHF(31, "CHF", "Swiss Franc", R.drawable.ch),
    THB(32, "THB", "Thai Baht", R.drawable.th),
    TRY(33, "TRY", "Turkish Lira", R.drawable.tr);


    companion object {
        fun getCurrencyById(id : String) : Currency? {
            for (value in values()) {
                if(value.shortName == id){
                    return value
                }
            }

            return null
        }
    }
}