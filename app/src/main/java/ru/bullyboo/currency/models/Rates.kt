package ru.bullyboo.currency.models

import ru.bullyboo.currency.enums.Currency

data class Rates(
    val base: String,
    val date: String,
    var ratesList: List<Rate>
)

data class Rate(
        val currency : Currency,
        val value : Double
)