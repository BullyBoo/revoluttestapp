package ru.bullyboo.currency

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import ru.bullyboo.currency.dagger.components.DaggerAppComponent
import timber.log.Timber

class App : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())
    }

    override fun applicationInjector(): AndroidInjector<App> = DaggerAppComponent.builder()
            .application(this)
            .build()

}