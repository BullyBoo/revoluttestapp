package ru.bullyboo.currency.adapters;

import android.support.annotation.LayoutRes;

import com.arellomobile.mvp.MvpPresenter;

public class ItemBuilder<T>{

    public interface Comparator<T>{
        boolean compare(T o1, T o2);
    }

    public interface Identifier<T>{
        long getId(T o);
    }

    Identifier<T> identifier;

    @LayoutRes
    int layoutId = 0;

    int bindingId = 0;

    int presenterId = 0;

    Class<T> cClass;

    MvpPresenter presenter;

    Comparator<T> comparator;

    public ItemBuilder(Class<T> cClass) {
        this.cClass = cClass;
    }

    public ItemBuilder<T> setIdentifier(Identifier<T> identifier) {
        this.identifier = identifier;
        return this;
    }

    public ItemBuilder<T> layout(@LayoutRes int id) {
        layoutId = id;
        return this;
    }

    public ItemBuilder<T> bindingId(int id){
        bindingId = id;
        return this;
    }

    public ItemBuilder<T> presenterId(int presenterId) {
        this.presenterId = presenterId;
        return this;
    }

    public ItemBuilder<T> presenter(MvpPresenter presenter){
        this.presenter = presenter;
        return this;
    }

    public ItemBuilder<T> comparator(Comparator<T> comparator) {
        this.comparator = comparator;
        return this;
    }

    public ItemBuilder<T> build() {
        return this;
    }

}