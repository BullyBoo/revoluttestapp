package ru.bullyboo.currency.adapters;

import java.util.ArrayList;
import java.util.List;

public class Builder{

    private List<ItemBuilder> list = new ArrayList<>();

    Builder() {

    }

    public Builder newType(ItemBuilder builder) {
        list.add(builder);
        return this;
    }

    @SuppressWarnings("unchecked")
    public BindingAdapter build() {
        return new BindingAdapter(list);
    }
}