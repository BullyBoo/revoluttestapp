package ru.bullyboo.currency.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class BindingAdapter extends RecyclerView.Adapter<BindingAdapter.BindingHolder> {

    private WeakReference<LayoutInflater> layoutInflaterReference;

    private List<ItemBuilder> builderList;
    private List<Object> itemList;

    BindingAdapter(List<ItemBuilder> builderList) {
        this.builderList = builderList;

        itemList = new ArrayList<>();
    }

    @NonNull
    @Override
    public BindingHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (layoutInflaterReference == null || layoutInflaterReference.get() == null) {
            layoutInflaterReference = new WeakReference<>(LayoutInflater.from(viewGroup.getContext()));
        }

        LayoutInflater layoutInflater = layoutInflaterReference.get();

        View view = layoutInflater.inflate(builderList.get(viewType).layoutId, viewGroup, false);

        return new BindingHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BindingHolder holder, int position) {
        Object item = itemList.get(position);

        int type = getItemViewType(position);

        if(type != -1){
            ItemBuilder builderType = builderList.get(type);

            holder.binding.setVariable(builderType.bindingId, item);
            holder.binding.setVariable(builderType.presenterId, builderType.presenter);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public long getItemId(int position) {
        int type = getItemViewType(position);

        if(type != -1){
            ItemBuilder itemBuilder = builderList.get(getItemViewType(position));

            Object item = itemList.get(position);

            return itemBuilder.identifier.getId(item);
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        for(ItemBuilder builder : builderList){
            if(itemList.size() > position && itemList.get(position) != null &&
                    itemList.get(position).getClass().equals(builder.cClass)){
                return builderList.indexOf(builder);
            }
        }
        return -1;
    }

    class BindingHolder extends RecyclerView.ViewHolder {

        ViewDataBinding binding;

        private BindingHolder(@NonNull View itemView) {
            super(itemView);

            binding = DataBindingUtil.bind(itemView);
        }
    }

    public void submitList(List<Object> list) {
        this.itemList = list;
        notifyDataSetChanged();

    }

    public static Builder newBuilder() {
        return new Builder();
    }
}
