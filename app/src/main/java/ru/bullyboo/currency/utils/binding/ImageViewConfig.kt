package ru.bullyboo.currency.utils.binding

import android.databinding.BindingAdapter
import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.bumptech.glide.Glide

object ImageViewConfig {

    @BindingAdapter("srcCompat")
    @JvmStatic
    fun srcCompat(view: ImageView, @DrawableRes drawableId : Int) {

        Glide.with(view)
                .load(drawableId)
                .into(view)
    }
}