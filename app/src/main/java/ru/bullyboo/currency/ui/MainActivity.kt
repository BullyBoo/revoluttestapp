package ru.bullyboo.currency.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import ru.bullyboo.currency.BR
import ru.bullyboo.currency.R
import ru.bullyboo.currency.adapters.BindingAdapter
import ru.bullyboo.currency.adapters.ItemBuilder
import ru.bullyboo.currency.enums.Currency
import ru.bullyboo.currency.interfaces.MainView
import ru.bullyboo.currency.models.Rate
import ru.bullyboo.currency.presenter.MainPresenter
import javax.inject.Inject

class MainActivity : MvpAppCompatActivity(), MainView {

    @Inject
    @InjectPresenter
    lateinit var presenter : MainPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = BindingAdapter.newBuilder()
                .newType(ItemBuilder(Currency::class.java)
                        .setIdentifier { it.id }
                        .bindingId(BR.currency)
                        .comparator { r1, r2 -> r1.id == r2.id}
                        .layout(R.layout.item_base_currency)
                        .presenter(presenter)
                        .presenterId(BR.presenter)
                        .build()
                )
                .newType(ItemBuilder(Rate::class.java)
                        .setIdentifier { it.currency.id}
                        .bindingId(BR.rate)
                        .comparator { r1, r2 ->
                            r1.currency == r2.currency &&
                                    r1.value == r2.value
                        }
                        .layout(R.layout.item_currency)
                        .presenter(presenter)
                        .presenterId(BR.presenter)
                        .build()
                )
                .build()

        adapter.setHasStableIds(true)

        recycler.adapter = adapter

        presenter.subscribe()
                .observe(this, Observer{
                    if(it != null && !it.isEmpty()){
                        (adapter as BindingAdapter).submitList(it)
                    }
                })

        presenter.countDigit
                .observe(this, Observer {
                    adapter.notifyDataSetChanged()
                })
    }

    override fun onStart() {
        super.onStart()

        registerReceiver(presenter.receiver, presenter.intentFilter)
    }

    override fun onStop() {
        super.onStop()

        unregisterReceiver(presenter.receiver)
    }

    override fun setRecyclerVisibility(visibility: Int) {
        recycler.visibility = visibility
    }

    override fun setDescriptionVisibility(visibility: Int) {
        description.visibility = visibility
    }

    override fun setProgressVisibility(visibility: Int) {
        progress.visibility = visibility
    }

    override fun showToast(stringId: Int) {
        Toast.makeText(this, stringId, Toast.LENGTH_LONG).show()
    }

}
