package ru.bullyboo.currency.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager

/**
 * Ресивер, получающий сообщения о изменении состоянии сети
 */
class ConnectionReceiver (private val stateChangeListener : ConnectionReceiver.OnStateChanged) : BroadcastReceiver() {

    interface OnStateChanged{
        fun onChanged(status : Status)
    }

    enum class Status{
        NO_INFO,
        CONNECTED,
        NO_CONNECTION,
    }

    override fun onReceive(context : Context?, intent : Intent?) {

        if(intent?.action == "android.net.conn.CONNECTIVITY_CHANGE"){
            val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE)

            if(cm is ConnectivityManager ){
                val netInfo = cm.activeNetworkInfo
                val connected = netInfo != null && netInfo.isConnected

                stateChangeListener.onChanged(getStatus(connected))
            }
        }

    }

    private fun getStatus(connected : Boolean) : Status {
        return if(connected){
            Status.CONNECTED
        } else {
            Status.NO_CONNECTION
        }
    }

}