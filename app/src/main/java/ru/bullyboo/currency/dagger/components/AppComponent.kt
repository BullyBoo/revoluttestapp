package ru.bullyboo.currency.dagger.components

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import ru.bullyboo.currency.dagger.modules.ActivityModule
import ru.bullyboo.currency.dagger.modules.NetworkModule
import ru.bullyboo.currency.App
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,
    ActivityModule::class,
    NetworkModule::class])
internal interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    @Singleton
    interface Builder {

        @BindsInstance
        fun application(app: App): Builder

        fun build(): AppComponent
    }
}
