package ru.bullyboo.currency.dagger.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import ru.bullyboo.currency.ui.MainActivity
import ru.bullyboo.currency.dagger.annotations.ActivityScope

@Module(includes = [AndroidSupportInjectionModule::class])
interface ActivityModule{

    @ContributesAndroidInjector()
    @ActivityScope
    fun mainActivity(): MainActivity

}