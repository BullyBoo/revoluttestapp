package ru.bullyboo.currency.dagger.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.bullyboo.currency.constants.Network
import ru.bullyboo.currency.models.Rates
import ru.bullyboo.currency.network.RateApi
import ru.bullyboo.currency.network.converters.RateConverter
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient() : OkHttpClient =
            OkHttpClient.Builder()
                    .writeTimeout(40, TimeUnit.SECONDS)
                    .readTimeout(40, TimeUnit.SECONDS)
                    .connectTimeout(40, TimeUnit.SECONDS)
                    .addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { Timber.d(it) }).apply {
                        this.level = okhttp3.logging.HttpLoggingInterceptor.Level.BODY
                    })
                    .build()

    @Provides
    @Singleton
    fun provideGson() : Gson =
            GsonBuilder()
                    .registerTypeAdapter(Rates::class.java, RateConverter())
                    .create()

    @Provides
    @Singleton
    fun provideRetrofit(client : OkHttpClient, gson : Gson) : Retrofit =
            Retrofit.Builder()
                    .baseUrl(Network.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .build()

    @Provides
    @Singleton
    fun provideRateApi(retrofit : Retrofit) = retrofit.create(RateApi::class.java)!!
}