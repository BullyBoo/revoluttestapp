package ru.bullyboo.currency.network

class Response<M> (var status: Status = Status.LOADING, var model: M?, var error: Throwable? = null){

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }

    companion object {

        fun <T> success(model: T?): Response<T> {
            return Response(Status.SUCCESS, model)
        }

        fun <T> error(msg: Throwable?, model: T?): Response<T> {
            return Response(Status.ERROR, model, msg)
        }

        fun <T> loading(model: T?): Response<T> {
            return Response(Status.LOADING, model)
        }

    }
}