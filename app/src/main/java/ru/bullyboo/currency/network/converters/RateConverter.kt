package ru.bullyboo.currency.network.converters

import com.google.gson.*
import ru.bullyboo.currency.enums.Currency
import ru.bullyboo.currency.models.Rate
import ru.bullyboo.currency.models.Rates
import java.lang.reflect.Type

class RateConverter : JsonDeserializer<Rates>{

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Rates {

        if(json is JsonObject){
            val base = getString("base", json)
            val date = getString("date", json)

            val ratesElement = json.get("rates")

            val list = ArrayList<Rate>()

            if(ratesElement is JsonObject){

                ratesElement.entrySet().forEach {
                    list.add(Rate(Currency.getCurrencyById(it.key)!!,
                            getDouble(it.value)))
                }
            }

            return Rates(base, date, list)
        }

        return Rates("", "", null!!)
    }

    private fun getString(key : String, obj : JsonObject) : String {
        val element = obj.get(key)

        if(element is JsonPrimitive){
            return element.asString
        }

        return ""
    }

    private fun getDouble(element : JsonElement) : Double {
        if(element is JsonPrimitive){
            return element.asDouble
        }

        return 0.0
    }

}