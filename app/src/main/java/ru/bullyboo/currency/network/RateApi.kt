package ru.bullyboo.currency.network

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import ru.bullyboo.currency.models.Rates

interface RateApi {

    @GET("latest?")
    fun getRate(
            @Query("base") baseCurrency : String
    ) : Observable<Rates>

}