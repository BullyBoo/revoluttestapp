package ru.bullyboo.currency.network

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class  Request<M> {

    private var apiCall: (() -> Observable<M>)? = null

    private var resource = MutableLiveData<Response<M>>()

    private var lastSuccessModel : M? = null

    fun process(): LiveData<Response<M>> {
        resource.setValue(Response.loading(null))

        apiCall!!()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { m -> resource.value = Response.success(m)
                            lastSuccessModel = m},
                        { t -> resource.setValue(Response.error(t, lastSuccessModel)) })
        return resource
    }


    fun setApiCall(apiCall : () -> Observable<M>): Request<M> {
        this.apiCall = apiCall

        return this
    }

    fun getLiveData() = resource
}
